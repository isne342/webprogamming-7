-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 30, 2016 at 01:07 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `Calendar`
--

-- --------------------------------------------------------

--
-- Table structure for table `data`
--

CREATE TABLE `data` (
  `date` date NOT NULL,
  `title` text NOT NULL,
  `detail` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `data`
--

INSERT INTO `data` (`date`, `title`, `detail`) VALUES
('2016-09-30', 'MDL', 'Newbee Vs OG Dota2\r\nTeam Secret Vs EG\r\nMVP.P Vs VG\r\nLGD.Forever Young Vs IG.V'),
('2016-10-01', 'Premier League', '02:00 Everton  Vs Crystal Palace\r\n18:30 Swansea City Vs Liverpool\r\n21:00 Hull City Vs Chelsea\r\n21:00 Sunderland Vs WBA\r\n21:00 Watford Vs AFC Bournemouth\r\n21:00 West Ham United Vs Middlesbrough'),
('2016-10-02', 'Premier League', '18:00 Manchester United Vs Stoke City\r\n20:15 Leicester City Vs Southampton\r\n20:15 Tottenham Hotspur Vs Manchester City\r\n22:30 Burnley Vs Arsenal'),
('2016-10-03', 'Eng exam', '15:30 - 18:30\r\n\r\nHB 502'),
('2016-10-04', 'Algo exam', ''),
('2016-10-05', 'Network exam', 'Aj . Aunya 12:00-15:00'),
('2016-10-07', 'Stat IE exam', ''),
('2016-10-08', 'Network exam', 'Aj. Yuttapong'),
('2016-10-10', 'Web pro exam', '13:00-15:00');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
